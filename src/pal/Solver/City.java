package pal.Solver;

import java.util.ArrayList;
import java.util.Collection;

public class City {

    public static int MAX_GAS_TANK_SIZE;

    private int index;

    private IGasStationState stateGasStation;

    private int bestTankValue;

    private Collection<Highway> adjacentHighways;

    interface IGasStationState {
        int getAmountOfGasInTank();
        void markVisit();
        boolean isVisited(int currentGasInTank);
    }

    class HasGasStation implements IGasStationState {
        private boolean visited = false;
        City city;


        HasGasStation(City city) {
            this.city = city;
        }

        @Override
        public int getAmountOfGasInTank() {
            return city.MAX_GAS_TANK_SIZE;
        }

        @Override
        public void markVisit() {
            this.visited = true;
        }

        @Override
        public boolean isVisited(int currentGasInTank) {
            if (currentGasInTank < 0) {
                return true;
            }

            return this.visited;
        }
    }

    class HasNotGasStation implements IGasStationState {
        City city;

        HasNotGasStation(City city) {
            this.city = city;
        }

        @Override
        public int getAmountOfGasInTank() {
            return city.bestTankValue;
        }

        @Override
        public void markVisit() {
            // nop
        }

        @Override
        public boolean isVisited(int currentGasInTank) {
            if (currentGasInTank > this.city.bestTankValue) {
                this.city.bestTankValue = currentGasInTank;
                return false;
            }

            return true;
        }
    }

    public City(int index) {
        this.bestTankValue = 0;
        this.stateGasStation = new HasGasStation(this);
        this.adjacentHighways = new ArrayList<Highway>(5);
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setGasStation(boolean isGasStation) {
        if (isGasStation) {
            this.stateGasStation = new HasGasStation(this);
        } else {
            this.stateGasStation = new HasNotGasStation(this);
        }
    }

    public void addAdjacentCity(City city, int length) {
        this.adjacentHighways.add(new Highway(city, length));
    }

    public Collection<Highway> getHighways() {
        return this.adjacentHighways;
    }

    public void markVisit() {
        this.stateGasStation.markVisit();
    }

    public boolean isVisited(int gasInTank) {
        return this.stateGasStation.isVisited(gasInTank);
    }

    public int getAmountOfGasInTank() {
        return this.stateGasStation.getAmountOfGasInTank();
    }
}
