package pal.Solver;

import pal.InputParser.InputContainer;

import java.util.PriorityQueue;

public class Solver {
    class QueueItem implements Comparable<QueueItem> {

        City city;
        int cost;
        int currentGas;

        QueueItem(City city, int cost, int currentGas) {
            this.city = city;
            this.cost = cost;
            this.currentGas = currentGas;
        }

        @Override
        public int compareTo(QueueItem o) {
            if (this.cost < o.cost) {
                return -1;
            } else if (this.cost > o.cost) {
                return 1;
            } else {
                return 0;
            }
        }

        @Override
        public String toString() {
            return "QueueItem{" +
                    "city=" + city.getIndex() +
                    ", cost=" + cost +
                    ", currentGas=" + currentGas +
                    '}';
        }
    }

    InputContainer input;


    public int solve(InputContainer inputContainer) {
        City.MAX_GAS_TANK_SIZE = inputContainer.getTankSize();

        this.input = inputContainer;

        PriorityQueue<QueueItem> priorityQueue = new PriorityQueue<QueueItem>();

        City startCity = inputContainer.getStartCity();

        int totalLength = 0;
        this.loadAdjacentCitiesIntoQueue(startCity, priorityQueue, totalLength);
        startCity.markVisit();
        City currentCity = startCity;

        while (true) {
            QueueItem item = priorityQueue.poll();

            if (item.city.isVisited(item.currentGas)) {
                continue;
            }

            currentCity = item.city;
            totalLength = item.cost;

            if (currentCity.getIndex() == inputContainer.getEndCity().getIndex()) {

                break;
            }

            this.loadAdjacentCitiesIntoQueue(currentCity, priorityQueue, totalLength);
            currentCity.markVisit();
        }

        return totalLength;
    }

    private void loadAdjacentCitiesIntoQueue(City originCity, PriorityQueue<QueueItem> queue, int lengthToOriginCity) {
        for(Highway highway: originCity.getHighways()) {
            queue.add(
                new QueueItem(
                    highway.getTargetCity(),
                    lengthToOriginCity + highway.getLength(),
                    originCity.getAmountOfGasInTank() - highway.getLength()
                )
            );
        }
    }
}
