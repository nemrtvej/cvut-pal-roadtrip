package pal.Solver;

public class Highway {
    private City targetCity;

    private int length;

    public Highway(City targetCity, int length) {
        this.targetCity = targetCity;
        this.length = length;
    }

    public City getTargetCity() {
        return this.targetCity;
    }

    public int getLength() {
        return this.length;
    }
}
