package pal.InputParser;

import pal.Solver.City;

public class InputContainer {

    private City[] cities;
    private City[] citiesWithoutGas;
    private City startCity;
    private City endCity;
    private int tankSize;

    public InputContainer(City[] cities, City[] citiesWithoutGas, City startCity, City endCity, int tankSize) {
        this.cities = cities;
        this.citiesWithoutGas = citiesWithoutGas;
        this.startCity = startCity;
        this.endCity = endCity;
        this.tankSize = tankSize;
    }

    public City[] getCitiesWithoutGas() {
        return citiesWithoutGas;
    }

    public City getStartCity() {
        return startCity;
    }

    public City getEndCity() {
        return endCity;
    }

    public int getTankSize() {
        return tankSize;
    }
}
