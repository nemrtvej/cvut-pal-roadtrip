package pal.InputParser;

import pal.Solver.City;

import java.io.BufferedReader;
import java.io.IOException;

public class Parser {

    public InputContainer parseInput(BufferedReader bufferedReader) throws IOException {

        int numberOfCitiesWithGas;
        int numberOfCitiesWithoutGas;
        int numberOfHighways;
        int tankCapacity;
        int startCityIndex;
        int endCityIndex;

        String inputLine;

        inputLine = bufferedReader.readLine();

        String[] parts = inputLine.split(" ");

        numberOfCitiesWithGas = Integer.parseInt(parts[0]);
        numberOfCitiesWithoutGas = Integer.parseInt(parts[1]);
        numberOfHighways = Integer.parseInt(parts[2]);
        tankCapacity = Integer.parseInt(parts[3]);
        startCityIndex = Integer.parseInt(parts[4]);
        endCityIndex = Integer.parseInt(parts[5]);

        City[] citiesWithoutGas = new City[numberOfCitiesWithoutGas];
        City[] citiesWithGas = new City[numberOfCitiesWithGas];
        City[] cities = createArrayOfCities(numberOfCitiesWithGas + numberOfCitiesWithoutGas);

        int citiesWithoutGasLastIndex = 0;
        int citiesWithGasLastIndex = 0;
        int lastReadCityIndex = -1;
        int cityIndex;

        for (int i = 0; i<numberOfCitiesWithGas; i++) {
            cityIndex = Integer.parseInt(bufferedReader.readLine());
            citiesWithGas[citiesWithGasLastIndex++] = cities[cityIndex];

            for (int j = lastReadCityIndex+1; j<cityIndex; j++) {
                citiesWithoutGas[citiesWithoutGasLastIndex++] = cities[j];
                cities[j].setGasStation(false);
            }

            lastReadCityIndex = cityIndex;
        }

        for (int i = lastReadCityIndex+1; i<cities.length; i++) {
            citiesWithoutGas[citiesWithoutGasLastIndex++] = cities[i];
            cities[i].setGasStation(false);
        }

        for (int i = 0; i<numberOfHighways; i++) {
            inputLine = bufferedReader.readLine();
            parts = inputLine.split(" ");
            int firstCityIndex = Integer.parseInt(parts[0]);
            int secondCityIndex = Integer.parseInt(parts[1]);
            int length = Integer.parseInt(parts[2]);

            cities[firstCityIndex].addAdjacentCity(cities[secondCityIndex], length);
            cities[secondCityIndex].addAdjacentCity(cities[firstCityIndex], length);
        }

        return new InputContainer(cities, citiesWithoutGas, cities[startCityIndex], cities[endCityIndex], tankCapacity);
    }

    private City[] createArrayOfCities(int numberOfCities) {
        City[] array = new City[numberOfCities];

        for (int i = 0; i<numberOfCities; i++) {
            array[i] = new City(i);
        }

        return array;
    }

}
