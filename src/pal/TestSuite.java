package pal;

import pal.InputParser.InputContainer;
import pal.InputParser.Parser;

import java.io.*;

public class TestSuite {

    private String directoryPath;

    class TestCase {
        InputContainer input;

        int solutionLength;

        TestCase(InputContainer input, int solutionLength) {
            this.input = input;
            this.solutionLength = solutionLength;
        }

        public InputContainer getInput() {
            return input;
        }

        public int getSolutionLength() {
            return solutionLength;
        }
    }

    public TestSuite(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    TestCase[] getTestCases() throws IOException{


        TestCase[] testCases =  new TestCase[10];

        for (int i = 1; i<11; i++) {
            testCases[i-1] = this.getTestCase("pub0"+i);
        }

        return testCases;
    }

    private TestCase getTestCase(String fileName) throws IOException {
        File inputFile = new File(this.directoryPath + "/" + fileName + ".in");
        File resultFile = new File(this.directoryPath + "/" + fileName + ".out");

        Parser parser = new Parser();

        BufferedReader bufferedInputReader = new BufferedReader(new FileReader(inputFile));
        InputContainer input = parser.parseInput(bufferedInputReader);

        BufferedReader bufferedResultReader = new BufferedReader(new FileReader(resultFile));
        String line = bufferedResultReader.readLine();
        int resultLength = Integer.parseInt(line);

        return new TestCase(input, resultLength);
    }
}
