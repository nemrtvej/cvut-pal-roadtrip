package pal;

import pal.InputParser.InputContainer;
import pal.InputParser.Parser;
import pal.Solver.Solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        InputStreamReader inputStream = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStream);

        Parser parser = new Parser();

        InputContainer inputContainer = parser.parseInput(bufferedReader);

        Solver solver = new Solver();

        System.out.println(solver.solve(inputContainer));
    }
}
