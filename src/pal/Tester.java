package pal;

import pal.InputParser.InputContainer;
import pal.Solver.Solver;

import java.io.IOException;

public class Tester {
    public static void main(String[] args) throws IOException{
        TestSuite testSuite = new TestSuite("c:/Phoenix/CVUT/PAL/Homeworks/3/src/pal/testCases/");

        TestSuite.TestCase[] testCases = testSuite.getTestCases();

        int i = 1;

        for (TestSuite.TestCase testCase: testCases) {
            try {

                System.out.println("Test case #"+i);
                Solver solver = new Solver();

                long startTime = System.currentTimeMillis();

                InputContainer inputContainer = testCase.getInput();

                int solution = solver.solve(inputContainer);

                long endTime = System.currentTimeMillis() - startTime;

                System.out.println("Expected result: "+testCase.getSolutionLength());
                System.out.println("Computed result: "+solution);
                System.out.println("Computation time: "+ (endTime/1000.0) + "s");
                System.out.println("Result: " + (solution == testCase.getSolutionLength() ? "OK" : "FAIL"));

                System.out.println("--------------------------------------------------------------------");
                System.out.println("--------------------------------------------------------------------");
                System.out.println("--------------------------------------------------------------------");

            } catch (NullPointerException e) {
                System.out.println(e.getMessage());
            } finally {
                i++;
            }

        }
    }
}
